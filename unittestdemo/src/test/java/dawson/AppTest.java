package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void testEcho() {
        assertEquals("Testing the echo method from the Application class", 5, App.echo(5));
    }

    @Test
    public void testOneMore() {
        assertEquals("Testing the oneMore method from the Application class", 6, App.echo(5));
    }
}
